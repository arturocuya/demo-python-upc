class Node:
    leftChild = None
    rightChild = None

    def __init__(self, value):
        self.value = value

    def __str__(self):
        return str(self.value)

    def weight(self):
        if not self.leftChild and not self.rightChild:
            return self.value
        else:
            weight = self.value
            if self.leftChild:
                weight += self.leftChild.weight()

            if self.rightChild:
                weight += self.rightChild.weight()
            
            return weight

def foo(value):
    return value

class Tree:
    def __init__(self, root):
        self.root = root

    def weight(self):
        return self.root.weight()

t = Tree(Node(3))
t.root.rightChild = Node(1)
t.root.leftChild = Node(1)

print(t.weight() == 5)

# MI COMENTARIO :AHAHDKJSHDLASJHKDJHSAJKHD
